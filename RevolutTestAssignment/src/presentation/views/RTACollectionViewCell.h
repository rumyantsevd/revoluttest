//
//  RTACollectionViewCell.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTACollectionViewCell : UICollectionViewCell

@property(nonatomic, copy) NSString *currencyText;
@property(nonatomic, copy) NSString *amountText;
@property(nonatomic, assign) BOOL amountError;

@end

//
//  RTACarouselView.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RTACarouselViewDataSource;

@interface RTACarouselView : UIView

+ (instancetype)create;

- (void)registerItemsNib:(UINib *)nib;
- (void)reload;

@property(nonatomic, assign) NSUInteger numberOfItems;
@property(nonatomic, copy) void(^didChangePage)(NSUInteger pageIndex);
@property(nonatomic, copy) void(^updateCellWithIndex)(UICollectionViewCell *cell, NSUInteger index);

@end

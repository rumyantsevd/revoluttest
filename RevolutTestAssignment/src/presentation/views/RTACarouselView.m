//
//  RTACarouselView.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTACarouselView.h"

static NSString *const kCarouselElementId = @"kCarouselElementId";

@interface RTACarouselView () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation RTACarouselView

+ (instancetype)create {
    NSBundle *bundle = [NSBundle mainBundle];
    return [bundle loadNibNamed:NSStringFromClass(self)
                          owner:nil
                        options:nil].firstObject;
}

//TODO: don't forget to transfer constraints in case of using autolayout
- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder {
    BOOL validInstanceReceiver = [self subviews].count != 0;
    if (!validInstanceReceiver) {
        RTACarouselView *view = [RTACarouselView create];
        view.frame = self.frame;
        view.autoresizingMask = self.autoresizingMask;
        view.alpha = self.alpha;
        view.backgroundColor = self.backgroundColor;
        view.hidden = self.hidden;
        return view;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
}

- (void)setNumberOfItems:(NSUInteger)numberOfItems {
    NSUInteger oldNumber = _numberOfItems;
    if (_numberOfItems != numberOfItems) {
        _numberOfItems = numberOfItems;
        [self reload];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (oldNumber == 0) {
                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_numberOfItems * 5000 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
            }
        });
    }
}

- (void)reload {
    [self.collectionView reloadData];
}


- (void)registerItemsNib:(UINib *)nib {
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:kCarouselElementId];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    // it is a pseudo infinite collection, but it will takes more than an hour
    // to swipe collection till the end
    return 10000 * _numberOfItems;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(collectionView.bounds), CGRectGetHeight(collectionView.bounds));
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCarouselElementId forIndexPath:indexPath];
    if (_updateCellWithIndex) {
        _updateCellWithIndex(cell, [self indexInExternalCollectionFromCollectionViewIndex:indexPath.row]);
    }
    return cell;
}

#pragma mark - UIScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = _collectionView.frame.size.width;
    NSInteger currentPage = (NSInteger)floor((_collectionView.contentOffset.x - pageWidth / 2 ) / pageWidth) + 1;
    NSUInteger selectedIndex = [self indexInExternalCollectionFromCollectionViewIndex:currentPage];
    if (_didChangePage) {
        _didChangePage(selectedIndex);
    }
}


#pragma mark - Private

- (NSInteger)indexInExternalCollectionFromCollectionViewIndex:(NSInteger)collectionViewIndex {
    if (_numberOfItems == 0) {
        return 0;
    }
    return collectionViewIndex % _numberOfItems;
}

@end

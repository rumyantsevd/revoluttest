//
//  RTACollectionViewCell.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTACollectionViewCell.h"

@interface RTACollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

@implementation RTACollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCurrencyText:(NSString *)currencyText {
    _currencyText = [currencyText copy];
    _currencyLabel.text = currencyText;
}

- (void)setAmountText:(NSString *)amountText {
    _amountText = [amountText copy];
    _amountLabel.text = amountText;
}

- (void)setAmountError:(BOOL)amountError {
    _amountError = amountError;
    _amountLabel.textColor = _amountError ? [UIColor redColor] : [UIColor whiteColor];
}

@end

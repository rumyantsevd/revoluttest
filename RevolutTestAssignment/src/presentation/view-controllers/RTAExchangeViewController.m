//
//  RTAExchangeViewController.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAExchangeViewController.h"
#import "RTACarouselView.h"
#import "RTACollectionViewCell.h"
#import <RZDataBinding.h>
#import "RTAExchangeViewModel.h"
#import "RTACurrencyViewDTO.h"

@interface RTAExchangeViewController ()

@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property(nonatomic, strong) id<RTAExchangeViewModel> viewModel;

@property (weak, nonatomic) IBOutlet UITextField *fromCurrencyTextField;

@property (weak, nonatomic) IBOutlet UILabel *toCurrencyLabel;
@property (weak, nonatomic) IBOutlet RTACarouselView *fromCurrencyCarousel;
@property (weak, nonatomic) IBOutlet RTACarouselView *toCurrencyCarousel;
@property (weak, nonatomic) IBOutlet UILabel *exchangeRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *minusLabel;
@property (weak, nonatomic) IBOutlet UILabel *plusLabel;
@property (weak, nonatomic) IBOutlet UITextField *toCurrencyTextField;
@property (weak, nonatomic) IBOutlet UIPageControl *fromPageControl;
@property (weak, nonatomic) IBOutlet UIPageControl *toPageControl;

@end

@implementation RTAExchangeViewController

- (instancetype)initWithRTAExchangeViewModel:(id<RTAExchangeViewModel>)viewModel {
    NSCParameterAssert(viewModel);
    if (!viewModel) {
        return nil;
    }
    if (self) {
        _viewModel = viewModel;
    }
    return self;
}
- (IBAction)exchangeButtonPressed:(id)sender {
    [_viewModel handleExchangeButtonPressed];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([RTACollectionViewCell class]) bundle:nil];
    [_fromCurrencyCarousel registerItemsNib:cellNib];
    [_toCurrencyCarousel registerItemsNib:cellNib];

    @weakify(self);
    [_fromCurrencyCarousel setDidChangePage:^(NSUInteger pageIndex){
        @strongify(self);
        if (self) {
            [self.fromPageControl setCurrentPage:pageIndex];
            [self->_viewModel handleFromCurrencySelectedAtIndex:pageIndex];
        }
    }];
    [_toCurrencyCarousel setDidChangePage:^(NSUInteger pageIndex){
        @strongify(self);
        if (self) {
            [self.toPageControl setCurrentPage:pageIndex];
            [self->_viewModel handleToCurrencySelectedAtIndex:pageIndex];
        }
    }];
    [self configureCellUpdateBlocksWithCarousel:_fromCurrencyCarousel fromAccount:YES];
    [self configureCellUpdateBlocksWithCarousel:_toCurrencyCarousel fromAccount:NO];

    [_exchangeButton rz_bindKey:@keypath(_exchangeButton,enabled)
                      toKeyPath:@keypath(_viewModel,exchangeButtonEnabled)
                       ofObject:_viewModel];
    [_exchangeRateLabel rz_bindKey:@keypath(_exchangeRateLabel,text)
                         toKeyPath:@keypath(_viewModel,exchangeRateString)
                          ofObject:_viewModel];

    [self reloadCarousels];

    [_viewModel setDidUpdateBlock:^{
        @strongify(self);
        [self reloadCarousels];
    }];
    [_viewModel setFromAmountStringChanged:^(NSString *amount){
        @strongify(self);
        self.fromCurrencyTextField.text = amount;
        [self configureFromTextField];
    }];
    [_viewModel setToAmountStringChanged:^(NSString *amount){
        @strongify(self);
        self.toCurrencyTextField.text = amount;
        [self configureToTextField];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_fromCurrencyTextField becomeFirstResponder];
}

#pragma mark - Private 

- (void)reloadCarousels {
    [_fromPageControl setNumberOfPages:[_viewModel fromCurrencyViewDTO].count];
    [_toPageControl setNumberOfPages:[_viewModel toCurrencyViewDTO].count];

    [_fromCurrencyCarousel setNumberOfItems:[_viewModel fromCurrencyViewDTO].count];
    [_toCurrencyCarousel setNumberOfItems:[_viewModel toCurrencyViewDTO].count];

    [_fromCurrencyCarousel reload];
    [_toCurrencyCarousel reload];
}

- (IBAction)fromAmountChanged:(id)sender {
    [self configureFromTextField];
    [_viewModel handleFromAmountStringChanged:_fromCurrencyTextField.text];
}

- (IBAction)toAmountChanged:(id)sender {
    [self configureToTextField];
    [_viewModel handleToAmountStringChanged:_toCurrencyTextField.text];
}

- (void)configureCellUpdateBlocksWithCarousel:(RTACarouselView *)carousel
                                    fromAccount:(BOOL)fromAccount {
    @weakify(self);
    [carousel setUpdateCellWithIndex:^(UICollectionViewCell *cell, NSUInteger index){
        @strongify(self);
        if (self) {
            NSArray *data = fromAccount ? [self->_viewModel fromCurrencyViewDTO] : [self->_viewModel toCurrencyViewDTO];
            RTACollectionViewCell *collectionViewCell = (RTACollectionViewCell *)cell;
            RTACurrencyViewDTO *viewDTO = data[index];
            collectionViewCell.currencyText = viewDTO.currencyString;
            collectionViewCell.amountText = viewDTO.currencyAmountString;
            collectionViewCell.amountError = viewDTO.amountError;
        }
    }];
}

- (void)configureFromTextField {
    [self layouTextField:_fromCurrencyTextField label:_minusLabel];
    _minusLabel.hidden = _fromCurrencyTextField.text.length == 0;
}

- (void)configureToTextField {
    [self layouTextField:_toCurrencyTextField label:_plusLabel];
    _plusLabel.hidden = _toCurrencyTextField.text.length == 0;
}

- (void)layouTextField:(UITextField *)textField label:(UILabel *)label {
    //TODO: use autolayout

        [textField sizeToFit];

        CGRect frame = textField.frame;
        frame.origin.x = CGRectGetWidth(self.view.bounds) - CGRectGetWidth(textField.frame) - 16;
        textField.frame = frame;

        CGRect labelFame = label.frame;
        labelFame.origin.x = CGRectGetMinX(textField.frame) - CGRectGetWidth(label.frame);
        label.frame = labelFame;

        label.hidden = textField.text.length == 0;


}


@end

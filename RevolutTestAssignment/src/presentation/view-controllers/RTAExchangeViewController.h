//
//  RTAExchangeViewController.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RTAExchangeViewModel;
@interface RTAExchangeViewController : UIViewController

- (instancetype)initWithRTAExchangeViewModel:(id<RTAExchangeViewModel>)viewModel;

@end

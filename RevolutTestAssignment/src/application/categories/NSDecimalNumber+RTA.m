//
//  NSDecimalNumber+RTA.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "NSDecimalNumber+RTA.h"

@implementation NSDecimalNumber (RTA)

- (BOOL)rta_lessThan:(NSDecimalNumber *)number {
    NSCParameterAssert(number);
    if (!number) {
        return NO;
    }
    return [self compare:number] == NSOrderedAscending;
}

- (BOOL)rta_GreaterThan:(NSDecimalNumber *)number {
    NSCParameterAssert(number);
    if (!number) {
        return NO;
    }
    return [self compare:number] == NSOrderedDescending;
}

@end

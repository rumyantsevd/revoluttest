//
//  NSDecimalNumber+RTA.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (RTA)

- (BOOL)rta_lessThan:(NSDecimalNumber *)number;
- (BOOL)rta_GreaterThan:(NSDecimalNumber *)number;

@end

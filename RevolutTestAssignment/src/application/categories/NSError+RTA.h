//
//  NSError+RTA.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kRevolutNetworkErrorDomain;
extern const NSUInteger kRevolutNetworkErrorFailedParsingCode;

extern NSString *const kRevolutApplicationErrorDomain;
extern const NSUInteger kRevolutApplicationErrorUnknownCurrency;
extern const NSUInteger kRevolutApplicationErrorUnknownCurrency;
extern const NSUInteger kRevolutApplicationErrorNotEnoughMoney;

@interface NSError (RTA)

+ (NSError *)rta_parsingError;

+ (NSError *)rta_unknownCurrencyError;
+ (NSError *)rta_notEnoughMoneyError;

@end

//
//  NSError+RTA.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "NSError+RTA.h"

NSString *const kRevolutNetworkErrorDomain = @"ru.revolut-test.network-eror";
const NSUInteger kRevolutNetworkErrorFailedParsingCode = 100;

NSString *const kRevolutApplicationErrorDomain = @"ru.revolut-test.application-eror";
const NSUInteger kRevolutApplicationErrorUnknownCurrency = 100;
const NSUInteger kRevolutApplicationErrorNotEnoughMoney = 101;

@implementation NSError (RTA)

+ (NSError *)rta_parsingError {
    return [NSError errorWithDomain:kRevolutNetworkErrorDomain
                               code:kRevolutNetworkErrorFailedParsingCode
                           userInfo:nil];
}

+ (NSError *)rta_unknownCurrencyError {
    return [NSError errorWithDomain:kRevolutApplicationErrorDomain
                               code:kRevolutApplicationErrorUnknownCurrency
                           userInfo:nil];
}

+ (NSError *)rta_notEnoughMoneyError {
    return [NSError errorWithDomain:kRevolutApplicationErrorDomain
                               code:kRevolutApplicationErrorNotEnoughMoney
                           userInfo:nil];

}

@end

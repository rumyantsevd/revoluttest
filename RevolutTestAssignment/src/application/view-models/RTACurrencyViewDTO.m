//
//  RTACurrencyViewDTO.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTACurrencyViewDTO.h"

@implementation RTACurrencyViewDTO

- (instancetype)initWithCurrencyString:(NSString *)currencyString
                  currencyAmountString:(NSString *)currencyAmountString
                           amountError:(BOOL)amountError {
    NSCParameterAssert(currencyString);
    NSCParameterAssert(currencyAmountString);
    if (!currencyAmountString || !currencyString) {
        return nil;
    }
    self = [super init];
    if (self) {
        _currencyString = [currencyString copy];
        _currencyAmountString = [currencyAmountString copy];
        _amountError = amountError;
    }
    return self;
}





@end

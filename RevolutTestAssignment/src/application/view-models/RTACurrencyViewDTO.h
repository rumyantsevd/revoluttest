//
//  RTACurrencyViewDTO.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTACurrencyViewDTO : NSObject

@property(nonatomic, readonly) NSString *currencyString;
@property(nonatomic, readonly) NSString *currencyAmountString;
@property(nonatomic, readonly) BOOL amountError;

- (instancetype)initWithCurrencyString:(NSString *)currencyString
                  currencyAmountString:(NSString *)currencyAmountString
                           amountError:(BOOL)amountError;

@end

NS_ASSUME_NONNULL_END

//
//  RTAExchangeViewModel.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RTACurrencyViewDTO;
@protocol RTAExchangeViewModel <NSObject>

- (void)startUpdating;

@property(nonatomic, readonly) BOOL exchangeButtonEnabled;

@property(nonatomic, copy) void (^didUpdateBlock)(void);

@property(nonatomic, readonly) NSArray<RTACurrencyViewDTO *> *fromCurrencyViewDTO;
@property(nonatomic, readonly) NSArray<RTACurrencyViewDTO *> *toCurrencyViewDTO;

@property(nonatomic, readonly) NSString *fromAmountString;
@property(nonatomic, copy) void (^fromAmountStringChanged)(NSString *amount);

@property(nonatomic, readonly) NSString *toAmountString;
@property(nonatomic, copy) void (^toAmountStringChanged)(NSString *amount);

@property(nonatomic, readonly) NSString *exchangeRateString;

- (void)handleFromAmountStringChanged:(NSString *)fromAmountString;
- (void)handleToAmountStringChanged:(NSString *)toAmountString;

- (void)handleFromCurrencySelectedAtIndex:(NSUInteger)index;
- (void)handleToCurrencySelectedAtIndex:(NSUInteger)index;
- (void)handleExchangeButtonPressed;

@end

//
//  RTAExchangeViewModelImpl.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTAExchangeViewModel.h"

@protocol RTAExchangeRateModel;
@protocol RTAAccountModel;
@protocol RTAConvertationModel;

@interface RTAExchangeViewModelImpl : NSObject <RTAExchangeViewModel>

- (instancetype)initWithExchangeModel:(id<RTAExchangeRateModel>)model
                         accountModel:(id<RTAAccountModel>)accountModel
                    convertationModel:(id<RTAConvertationModel>)convertationModel NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

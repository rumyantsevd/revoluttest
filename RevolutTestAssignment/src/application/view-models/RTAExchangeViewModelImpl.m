//
//  RTAExchangeViewModelImpl.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAExchangeViewModelImpl.h"
#import "RTAExchangeRateModel.h"
#import "RTAAccountModel.h"
#import "RTAConvertationModel.h"
#import "RTAAccountDTO.h"
#import "NSDecimalNumber+RTA.h"
#import "RTACurrencyViewDTO.h"

static NSTimeInterval updateInterval = 30.f;

@interface RTAExchangeViewModelImpl ()
@property(nonatomic, strong) id<RTAExchangeRateModel> exchangeModel;
@property(nonatomic, strong) id<RTAAccountModel> accountModel;
@property(nonatomic, strong) id<RTAConvertationModel> convertationModel;
@property(nonatomic, weak) NSTimer *updateTimer;

@property(nonatomic, copy, readwrite) NSString *fromAmountString;
@property(nonatomic, copy, readwrite) NSString *toAmountString;
@property(nonatomic, copy, readwrite) NSString *exchangeRateString;
@property(nonatomic, readwrite) BOOL exchangeButtonEnabled;

@property(nonatomic, readonly) RTAAccountDTO *selectedFromAccount;
@property(nonatomic, readonly) RTAAccountDTO *selectedToAccount;

@end

@implementation RTAExchangeViewModelImpl {
    BOOL _started;
    NSUInteger _selectedFromAccountIndex;
    NSUInteger _selectedToAccountIndex;

    NSDecimalNumber *_fromAmount;
    NSDecimalNumber *_toAmount;

    BOOL _toAmountChangedLast;
}

@synthesize toCurrencyViewDTO = _toCurrencyViewDTO,
fromCurrencyViewDTO = _fromCurrencyViewDTO,
didUpdateBlock = _didUpdateBlock,
fromAmountStringChanged = _fromAmountStringChanged,
toAmountStringChanged = _toAmountStringChanged;


- (instancetype)initWithExchangeModel:(id<RTAExchangeRateModel>)exchangeModel
                         accountModel:(id<RTAAccountModel>)accountModel
                    convertationModel:(id<RTAConvertationModel>)convertationModel {
    NSCParameterAssert(exchangeModel);
    NSCParameterAssert(accountModel);
    NSCParameterAssert(convertationModel);
    NSCAssert(accountModel.accounts.count >= 2, @"Too few accounts to exchange");

    if (!exchangeModel || !accountModel || !convertationModel || accountModel.accounts.count < 2) {
        return nil;
    }
    self = [super init];
    if (self) {
        _exchangeModel = exchangeModel;
        _accountModel = accountModel;
        _convertationModel = convertationModel;
    }
    return self;
}

#pragma mark - RTAExchangeViewModel

- (void)startUpdating {
    if (_started) {
        return;
    }
    [self updateViewDTOs];
    _started = YES;
    [self intenralUpdate];
}

- (void)handleExchangeButtonPressed {
    if (_fromAmount && _toAmount) {
        BOOL success = [_accountModel transferFromAccount:self.selectedFromAccount
                                                toAccount:self.selectedToAccount
                                                   amount:_fromAmount
                                             exchangeRate:[_exchangeModel exchangeRate]
                                                    error:nil];
        if (!success) {
            NSLog(@"Oopsy"); //TODO: forward error to UI
        }
        [self updateViewDTOs];

        _toAmount = nil;
        self.toAmountString = nil;

        _fromAmount = nil;
        self.fromAmountString = nil;

        [self updateExchangeButtonState];
    }
}


- (void)handleFromAmountStringChanged:(NSString *)fromAmountString {
    _toAmountChangedLast = NO;
    _fromAmountString = fromAmountString;
    if (fromAmountString.length == 0) {
        _fromAmount = nil;
    } else {
        _fromAmount = [NSDecimalNumber decimalNumberWithString:fromAmountString];
    }

    [self updateAmounts];
}

- (void)handleToAmountStringChanged:(NSString *)toAmountString {
    _toAmountChangedLast = YES;
    _toAmountString = toAmountString;
    if (toAmountString.length == 0) {
        _toAmount = nil;
    } else {
        _toAmount = [NSDecimalNumber decimalNumberWithString:toAmountString];
    }
    [self updateAmounts];
}

- (void)handleToCurrencySelectedAtIndex:(NSUInteger)index {
    _selectedToAccountIndex = index;
    [self updateExchangeButtonState];
    [self updateExchangeRateString];
    [self updateAmounts];
}

- (void)handleFromCurrencySelectedAtIndex:(NSUInteger)index {
    _selectedFromAccountIndex = index;
    [self updateExchangeButtonState];
    [self updateExchangeRateString];
    [self updateAmounts];
}

#pragma mark - Private

- (void)setFromAmountString:(NSString *)fromAmountString {
    _fromAmountString = [fromAmountString copy];
    if (self.fromAmountStringChanged) {
        self.fromAmountStringChanged(fromAmountString);
    }
}

- (void)setToAmountString:(NSString *)toAmountString {
    _toAmountString = [toAmountString copy];
    if (self.toAmountStringChanged) {
        self.toAmountStringChanged(toAmountString);
    }
}

- (void)handleLoadingCompleted {
    NSTimer *timer  = [NSTimer scheduledTimerWithTimeInterval:updateInterval
                                                       target:self
                                                     selector:@selector(intenralUpdate)
                                                     userInfo:nil
                                                      repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.updateTimer = timer;
    [self updateExchangeButtonState];
    [self updateExchangeRateString];
    [self updateViewDTOs];
    [self updateAmounts];
}

- (void)intenralUpdate {
    @weakify(self);
    [_exchangeModel loadExchangeRateCompleted:^(NSDictionary<NSString *,NSDecimalNumber *> * _Nonnull result) {
        @strongify(self);
        [self handleLoadingCompleted];
    } failed:^(NSError * _Nullable error) {
        @strongify(self);
        [self handleLoadingCompleted];
    }];
}

- (void)updateExchangeButtonState {
    BOOL differentAccounts = ![self.selectedToAccount isEqual:self.selectedFromAccount];
    BOOL hasExchangeRate = [_exchangeModel exchangeRate].count > 0;

    BOOL validAmount = [_fromAmount doubleValue] > 0 &&
    ![_fromAmount rta_GreaterThan:self.selectedFromAccount.amount];

    self.exchangeButtonEnabled = differentAccounts && hasExchangeRate && validAmount;
}

- (void)updateExchangeRateString {
    if (!self.selectedFromAccount || self.selectedToAccount || !_exchangeModel.exchangeRate) {
        self.exchangeRateString = nil;
        return;
    }
    NSDecimalNumber *exchangeValue = [_convertationModel rateOfCurrency:self.selectedFromAccount.currency
                                                     relativeToCurrency:self.selectedToAccount.currency
                                                           exchangeRate:_exchangeModel.exchangeRate
                                                                  error:nil];

    NSString *fromCurrencyString = [self amountStringWithAmount:[NSDecimalNumber decimalNumberWithString:@"1"]
                                                       currency:self.selectedFromAccount.currency];
    NSString *toCurrencyString = [self amountStringWithAmount:exchangeValue currency:self.selectedToAccount.currency];

    self.exchangeRateString = [NSString stringWithFormat:@"%@ = %@",fromCurrencyString,toCurrencyString];
}

- (void)updateViewDTOs {
    NSMutableArray *fromDTOs = @[].mutableCopy;
    NSMutableArray *toDTOs = @[].mutableCopy;
    [[_accountModel accounts] enumerateObjectsUsingBlock:^(RTAAccountDTO * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        RTACurrencyViewDTO *fromDTO = [self viewDTOFromAccount:obj fromAccount:YES];
        RTACurrencyViewDTO *toDTO = [self viewDTOFromAccount:obj fromAccount:NO];

        [fromDTOs addObject:fromDTO];
        [toDTOs addObject:toDTO];
    }];
    _fromCurrencyViewDTO = [fromDTOs copy];
    _toCurrencyViewDTO = [toDTOs copy];
    [self updateExchangeRateString];
    if (self.didUpdateBlock) {
        self.didUpdateBlock();
    }
}

- (RTACurrencyViewDTO *)viewDTOFromAccount:(RTAAccountDTO *)accountDTO
                               fromAccount:(BOOL)fromAccount {
    NSString *amountString = [self amountStringWithAmount:accountDTO.amount currency:accountDTO.currency];
    NSString *totalAmountString = [NSString stringWithFormat:@"You have %@",amountString];
    BOOL amountError = fromAccount && [_fromAmount rta_GreaterThan:self.selectedFromAccount.amount];

    return [[RTACurrencyViewDTO alloc] initWithCurrencyString:accountDTO.currency
                                         currencyAmountString:totalAmountString
                                                  amountError:amountError];
}

- (NSString *)amountStringWithAmount:(NSDecimalNumber *)amount
                            currency:(NSString *)currency {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    if (currency) {
        [formatter setCurrencyCode:currency];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        if (([amount doubleValue] - floor([amount doubleValue])) < 0.01) {
            [formatter setMaximumFractionDigits:0];
        }
    } else {
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setMaximumFractionDigits:2];
    }
    return [formatter stringFromNumber:amount];
}

- (void)updateAmounts {
    if (!_toAmountChangedLast) {
        if (!_fromAmount) {
            _toAmount = nil;
            self.toAmountString = nil;
        } else {
            _toAmount = [_convertationModel convertCurrency:self.selectedFromAccount.currency
                                                 toCurrency:self.selectedToAccount.currency
                                                     amount:_fromAmount
                                               exchangeRate:_exchangeModel.exchangeRate
                                                      error:nil];
            self.toAmountString = [self amountStringWithAmount:_toAmount currency:nil];

        }
    } else {
        if (!_toAmount) {
            _fromAmount = nil;
            self.fromAmountString = nil;
            return;
        } else {
            _fromAmount = [_convertationModel convertCurrency:self.selectedToAccount.currency
                                                   toCurrency:self.selectedFromAccount.currency
                                                       amount:_toAmount
                                                 exchangeRate:_exchangeModel.exchangeRate
                                                        error:nil];
            self.fromAmountString = [self amountStringWithAmount:_fromAmount currency:nil];
        }
    }
    [self updateViewDTOs];
    [self updateExchangeButtonState];
}

- (RTAAccountDTO *)selectedFromAccount {
    return [_accountModel accounts][_selectedFromAccountIndex];
}

- (RTAAccountDTO *)selectedToAccount {
    return [_accountModel accounts][_selectedToAccountIndex];
}


@end

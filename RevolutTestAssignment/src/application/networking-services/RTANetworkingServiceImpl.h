//
//  RTANetworkingServiceImpl.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTANetworkingService.h"

@interface RTANetworkingServiceImpl : NSObject <RTANetworkingService>

@end

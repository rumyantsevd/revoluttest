//
//  RTANetworkingServiceImpl.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTANetworkingServiceImpl.h"
#import <AFNetworking.h>
#import "NSError+RTA.h"
#import <XMLDictionary.h>
#import "RTAConstants.h"

#define RS_JSON_VALUE(OBJ, CLASS_NAME)  \
[(OBJ) isKindOfClass:[CLASS_NAME class]] ? OBJ : nil;


@interface RTANetworkingServiceImpl ()

@property(nonatomic, strong) AFURLSessionManager *sessionManager;

@end

@implementation RTANetworkingServiceImpl

- (instancetype)init {
    self = [super init];
    if (self) {
        [self configureSessionManager];
    }
    return self;
}

#pragma mark - Public

- (void)loadExchangeRateCompleted:(void (^)(NSDictionary<NSString *, NSDecimalNumber *> * _Nonnull))completed
                           failed:(void (^)(NSError * _Nullable))failed {

    void (^safeFailed)(NSError *) = ^(NSError *error) {
        if (failed) {
            failed(error);
        }
    };
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:kExchangeRateURLString]];
    NSURLSessionDataTask *task = [_sessionManager dataTaskWithRequest:request
                                                    completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                           if (error) {
                                                               safeFailed(error);
                                                           } else {
                                                               NSDictionary<NSString *, NSDecimalNumber *> *result = [self exchangDictionaryFromResponseObject:responseObject];
                                                               if (result.count == 0) {
                                                                   safeFailed([NSError rta_parsingError]);
                                                               } else {
                                                                   if (completed) {
                                                                       completed(result);
                                                                   }
                                                               }
                                                           }
                                                    }];
    [task resume];
}

#pragma mark - Private

- (void)configureSessionManager {
    _sessionManager = [AFURLSessionManager new];
    _sessionManager.responseSerializer = [AFXMLParserResponseSerializer new];
}

- (NSDictionary<NSString *, NSDecimalNumber *> *)exchangDictionaryFromResponseObject:(id)responseObject {
    if (![responseObject isKindOfClass:[NSXMLParser class]]) {
        return nil;
    }
    NSDictionary *responseDictionary = [[XMLDictionaryParser new] dictionaryWithParser:responseObject];
    responseDictionary = RS_JSON_VALUE(responseDictionary[@"Cube"], NSDictionary);
    responseDictionary = RS_JSON_VALUE(responseDictionary[@"Cube"], NSDictionary);

    NSArray *rates = RS_JSON_VALUE(responseDictionary[@"Cube"], NSArray);

    NSMutableDictionary *result = @{}.mutableCopy;
    [rates enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = RS_JSON_VALUE(obj, NSDictionary);
        NSString *currencyKey = RS_JSON_VALUE(dict[@"_currency"], NSString);
        NSString *rateString = RS_JSON_VALUE(dict[@"_rate"], NSString);
        if (rateString && currencyKey) {
            NSDecimalNumber *rateNumber = [NSDecimalNumber decimalNumberWithString:rateString];
            if (rateString) {
                result[currencyKey] = rateNumber;
            }
        }
    }];
    return result.copy;
}

@end

//
//  RTANetworkingService.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RTANetworkingService <NSObject>

- (void)loadExchangeRateCompleted:(nullable void (^ )(NSDictionary<NSString *, NSDecimalNumber *> *result))completed
                           failed:(nullable void (^)( NSError * _Nullable error))failed;

@end

NS_ASSUME_NONNULL_END

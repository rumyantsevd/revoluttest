//
//  RTAExchangeRateModelImpl.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTAExchangeRateModel.h"

@protocol RTANetworkingService;
@interface RTAExchangeRateModelImpl : NSObject <RTAExchangeRateModel>

- (instancetype)initWithNetworkingService:(id <RTANetworkingService>)networkingService NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

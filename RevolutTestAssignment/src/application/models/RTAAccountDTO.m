//
//  RTAAccountDTO.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAAccountDTO.h"

@implementation RTAAccountDTO

- (instancetype)initWithCurrecny:(NSString *)currency
                          amount:(NSDecimalNumber *)amount {
    NSCParameterAssert(currency);
    NSCParameterAssert(amount);
    if (!currency || !amount) {
        return nil;
    }
    self = [super init];
    if (self) {
        _currency = [currency copy];
        _amount = [amount copy];
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    if (![object isKindOfClass:[RTAAccountDTO class]]) {
        return NO;
    }
    return [self.currency isEqualToString:[object currency]] ;

}

@end


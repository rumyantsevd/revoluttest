//
//  RTAAccountDTO.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 19.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN;

@interface RTAAccountDTO : NSObject

@property(nonatomic, readonly) NSString *currency;
@property(nonatomic, readonly) NSDecimalNumber *amount;

- (instancetype)initWithCurrecny:(NSString *)currency
                          amount:(NSDecimalNumber *)amount NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END;

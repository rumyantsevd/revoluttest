//
//  RTAConvertationModelImpl.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAConvertationModelImpl.h"
#import "RTAConstants.h"
#import "NSError+RTA.h"

@implementation RTAConvertationModelImpl

- (NSDecimalNumber *)convertCurrency:(NSString *)currency
                          toCurrency:(NSString *)toCurrency
                              amount:(NSDecimalNumber *)amount
                        exchangeRate:(NSDictionary<NSString *,NSDecimalNumber *> *)exchangeRate
                               error:(NSError **)error {
    NSCParameterAssert(currency);
    NSCParameterAssert(toCurrency);
    NSCParameterAssert(amount);
    NSCParameterAssert(exchangeRate);
    if (!currency || !toCurrency || !amount || !exchangeRate) {
        return nil;
    }
    NSDecimalNumber *rate = [self rateOfCurrency:currency
                              relativeToCurrency:toCurrency
                                    exchangeRate:exchangeRate
                                           error:error];
    if (!!rate) {
        return [amount decimalNumberByMultiplyingBy:rate];
    }
    return nil;
}

- (NSDecimalNumber *)rateOfCurrency:(NSString *)currency
                 relativeToCurrency:(NSString *)toCurrency 
                       exchangeRate:(NSDictionary<NSString *,NSDecimalNumber *> *)exchangeRate 
                              error:(NSError *__autoreleasing *)error {

    NSCParameterAssert(currency);
    NSCParameterAssert(toCurrency);
    NSCParameterAssert(exchangeRate);
    if (!currency || !toCurrency || !exchangeRate) {
        return nil;
    }
    NSDecimalNumber *rate = nil;

    NSDecimalNumber *fromCurrencyRateToBase = [currency isEqualToString:kBaseCurrency] ?
    [NSDecimalNumber one] : exchangeRate[currency];

    NSDecimalNumber *toCurrencyRateToBase = [toCurrency isEqualToString:kBaseCurrency] ?
    [NSDecimalNumber one] : exchangeRate[toCurrency];

    if (fromCurrencyRateToBase && toCurrency) {
        rate = [toCurrencyRateToBase decimalNumberByDividingBy:fromCurrencyRateToBase];
    }

    if (!rate && error) {
        *error = [NSError rta_unknownCurrencyError];
    }
    return rate;
}

@end

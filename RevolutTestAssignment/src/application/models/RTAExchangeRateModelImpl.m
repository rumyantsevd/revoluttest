//
//  RTAExchangeRateModelImpl.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 17.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAExchangeRateModelImpl.h"
#import "RTANetworkingService.h"

@interface RTAExchangeRateModelImpl ()
@property(nonatomic, strong) id <RTANetworkingService> networkingService;
@end

@implementation RTAExchangeRateModelImpl
@synthesize loading = _loading, exchangeRate = _exchangeRate;

- (instancetype)initWithNetworkingService:(id <RTANetworkingService>)networkingService {
    NSCParameterAssert(networkingService);
    if (!networkingService) {
        return nil;
    }
    self = [super init];
    if (self) {
        _networkingService = networkingService;
    }
    return self;
}

#pragma mark - RTAExchangeRateModel

- (void)loadExchangeRateCompleted:(void (^)(NSDictionary<NSString *, NSDecimalNumber *> * _Nonnull))completed failed:(void (^)(NSError * _Nullable))failed {
    _loading = YES;
    @weakify(self);
    [_networkingService loadExchangeRateCompleted:^(NSDictionary<NSString *, NSDecimalNumber *> * _Nonnull result) {
        @strongify(self);
        if (self) {
            self->_loading = NO;
            self->_exchangeRate = result;
        }
        if (completed) {
            completed(result);
        }
    } failed:^(NSError * _Nullable error) {
        @strongify(self);
        if (self) {
            self->_loading = NO;
        }
        if (failed) {
            failed(error);
        }
    }];
}

@end

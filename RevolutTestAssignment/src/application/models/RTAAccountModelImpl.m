//
//  RTAAccountModelImpl.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAAccountModelImpl.h"
#import "RTAConvertationModel.h"
#import "NSError+RTA.h"
#import "NSDecimalNumber+RTA.h"
#import "RTAAccountDTO.h"

@interface RTAAccountModelImpl ()
@property(nonatomic, strong) id<RTAConvertationModel> convertationModel;
@end

@implementation RTAAccountModelImpl
@synthesize accounts = _accounts;

- (instancetype)initWithConvertationModel:(id<RTAConvertationModel>)converationModel {
    NSCParameterAssert(converationModel);
    if (!converationModel) {
        return nil;
    }
    self = [super init];
    if (self) {
        _convertationModel = converationModel;
        [self configureAccount];
    }
    return self;
}

#pragma mark - RTAAccountModel

- (BOOL)transferFromAccount:(RTAAccountDTO *)fromAccount
                  toAccount:(RTAAccountDTO *)toAccount
                     amount:(NSDecimalNumber *)amount
               exchangeRate:(NSDictionary<NSString *, NSDecimalNumber *> *)exchangeRate
                      error:(NSError *__autoreleasing *)error {
    NSCParameterAssert(fromAccount);
    NSCParameterAssert(toAccount);
    NSCParameterAssert(amount);
    NSCParameterAssert(exchangeRate);
    if (!fromAccount || !toAccount || !amount || !exchangeRate) {
        return NO;
    }
    NSDecimalNumber *toCurrencyAmout = [_convertationModel convertCurrency:fromAccount.currency
                                                                toCurrency:toAccount.currency
                                                                    amount:amount
                                                              exchangeRate:exchangeRate
                                                                     error:error];
    if (!toCurrencyAmout) {
        return NO;
    }
    if ([fromAccount.amount rta_lessThan:amount]) {
        if (error) {
            *error = [NSError rta_notEnoughMoneyError];
        }
        return NO;
    }
    NSDecimalNumber *newFromCurrencyAmount = [fromAccount.amount decimalNumberBySubtracting:amount];
    NSDecimalNumber *newToCurrencyAmount = [toAccount.amount decimalNumberByAdding:toCurrencyAmout];

    [self updateAccount:fromAccount withAmount:newFromCurrencyAmount];
    [self updateAccount:toAccount withAmount:newToCurrencyAmount];
    return YES;
}


#pragma mark - Private

- (void)configureAccount {
    NSMutableArray *result = @[].mutableCopy;
    NSArray *currencies = @[@"USD",@"GBP",@"EUR"];
    [currencies enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        RTAAccountDTO *dto = [[RTAAccountDTO alloc] initWithCurrecny:obj
                                                              amount:[NSDecimalNumber decimalNumberWithString:@"100"]];
        [result addObject:dto];
    }];
    _accounts = result.copy;
}

- (void)updateAccount:(RTAAccountDTO *)account withAmount:(NSDecimalNumber *)amount {
    NSCAssert([_accounts containsObject:account], @"unknown account");
    if (![_accounts containsObject:account]) {
        return;
    }
    NSMutableArray *mutableAccounts = _accounts.mutableCopy;
    NSUInteger indexOFAccount = [_accounts indexOfObject:account];

    RTAAccountDTO *updatedAccount = [[RTAAccountDTO alloc] initWithCurrecny:account.currency amount:amount];
    [mutableAccounts replaceObjectAtIndex:indexOFAccount withObject:updatedAccount];
    _accounts = mutableAccounts.copy;
}

@end

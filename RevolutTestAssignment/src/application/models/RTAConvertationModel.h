//
//  RTAConvertationModel.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RTAConvertationModel <NSObject>

// 1 currency = x toCurrency
- (NSDecimalNumber *)rateOfCurrency:(NSString *)currency
                 relativeToCurrency:(NSString *)toCurrency
                       exchangeRate:(NSDictionary<NSString *,NSDecimalNumber *> *)exchangeRate
                              error:(NSError **)error;

- (NSDecimalNumber *)convertCurrency:(NSString *)currency
                          toCurrency:(NSString *)toCurrency
                              amount:(NSDecimalNumber *)amount
                        exchangeRate:(NSDictionary<NSString *,NSDecimalNumber *> *)exchangeRate
                               error:(NSError **)error;

@end

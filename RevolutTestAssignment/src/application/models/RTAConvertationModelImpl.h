//
//  RTAConvertationModelImpl.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTAConvertationModel.h"

@interface RTAConvertationModelImpl : NSObject <RTAConvertationModel>

@end

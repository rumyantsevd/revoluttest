//
//  RTAAccountModelImpl.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTAAccountModel.h"

@protocol RTAConvertationModel;

@interface RTAAccountModelImpl : NSObject <RTAAccountModel>

- (instancetype)initWithConvertationModel:(id<RTAConvertationModel>)converationModel NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

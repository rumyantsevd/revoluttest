//
//  RTAAccountModel.h
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RTAAccountDTO;
@protocol RTAAccountModel <NSObject>

@property(nonatomic, readonly) NSArray<RTAAccountDTO *> *accounts;

- (BOOL)transferFromAccount:(RTAAccountDTO *)fromAccount
                  toAccount:(RTAAccountDTO *)toAccount
                     amount:(NSDecimalNumber *)amount
               exchangeRate:(NSDictionary<NSString *, NSDecimalNumber *> *)exchangeRate
                      error:(NSError *__autoreleasing *)error;

@end

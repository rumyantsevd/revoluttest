//
//  RTAConstants.m
//  RevolutTestAssignment
//
//  Created by d.rumiantsev on 18.07.17.
//  Copyright © 2017 app. All rights reserved.
//

#import "RTAConstants.h"

NSString *const kBaseCurrency = @"EUR";
NSString *const kExchangeRateURLString = @"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
